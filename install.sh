#!/bin/bash
# Copyright Grant Barton, 2018

# Help info:
show_help() {
	echo "Usage: $ ./install.sh [-hsidb]
		Copies the bundled dotfiles into the home directory.
		
		-h	display this help and quit
		-s	install the hard-coded list of server software from your repositories
		-i	install the hard-coded list of command-line software from your repositories
		-d	install tmux_client.conf instead of tmux.conf
		-b	backup current config files into ~/backups , prompt to overwrite if ~/backups already exists"
}

DESKTOP=0

while getopts hsidb opt ; do
	case $opt in
		h)
			# display help info and quit
			show_help
			exit 0
			;;
		s)
			echo "Installing fail2ban and ufw...."
			apt update && apt install -y fail2ban ufw
			;;
		i)	
			echo "Installing vim, tmux, weechat, htop, screenfetch and git...."
			apt update && apt install -y vim tmux weechat htop screenfetch git
			;;
		d)
			DESKTOP=1
			;;
		b)
			# backup any current conf files to ~/backups
			overwrite = "Y"
			if [ -e ~/config_backups ] ; then
				overwrite = "N"
				read -p "~/config_backups already exists, overwrite? (y/n)" choice
				case "$choice" in
					y|Y ) $overwrite="Y" ;;
					n|N ) $overwrite="N" ;;
					*   ) echo "Invalid, quitting" && exit 1 ;;
				esac
			fi

			# The following segment is very ugly: find a way to clean it up.
			# Case statements? A seperate function?
			echo "Backing up current configs into ~/backups...."
			if [ $overwrite == "Y" ] ; then	
				mkdir ~/config_backups
			
				if [ -f ~/.tmux.conf ] ; then
					mv ~/.tmux.conf ~/backups/.tmux.conf.old
				fi
	
				if [ -f ~/.vimrc ] ; then
					mv ~/.vimrc ~/backups/.vimrc.old
				fi

				if [ -f ~/.bashrc ] ; then
					mv ~/.bashrc ~/backups/.bashrc.old
				fi

				if [ -f ~/.bash_aliases ] ; then
					mv ~/.bash_aliases ~/backups/.bash_aliases.old
				fi

				if [ -f ~/.bash_profile ] ; then
					mv ~/.bash_profile ~/backups/.bash_profile.old
				fi

				if [ -f ~/.bash_logout ] ; then
					mv ~/.bash_logout ~/backups/.bash_logout.old
				fi

			fi
			;;
		*)
			# display help info, then quit with an error
			show_help >&2
			exit 1
			;;
	esac
done
shift "$((OPTIND-1))"

# copy dotfiles to the correct locations
echo "Moving dotfiles into the correct location..."
cp ./configs/bashrc ~/.bashrc
cp ./configs/bash_aliases ~/.bash_aliases
cp ./configs/bash_profile ~/.bash_profile
cp ./configs/bash_logout ~/.bash_logout
cp ./configs/vimrc ~/.vimrc

if [ "$DESKTOP" -eq 1 ] ; then
	cp ./configs/tmux_client.conf ~/.tmux.conf
else
	cp ./configs/tmux.conf ~/.tmux.conf
fi

if [ ! -d ~/.vim/colors/ ] ; then
	mkdir -p ~/.vim/colors
fi
cp ./vim-monokai/colors/monokai.vim ~/.vim/colors/monokai.vim
# configure vim for root:
mkdir -p /root/.vim/colors
cp ./vim-monokai/colors/monokai.vim /root/.vim/colors/monokai.vim
cp ./configs/vimrc /root/.vimrc

if [ ! -d ~/.weechat/ ] ; then
	mkdir ~/.weechat
fi
cp ./configs/weechat.irc.conf ~/.weechat/irc.conf

echo "Tasks finished"

exit 0
